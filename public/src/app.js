import { SVG, find } from '../contrib/svg-3.2.0.esm.min.js'
import { golden_dragon } from './golden_dragon.js'

const default_config = {
	size: 300,
	hairline: 1,
}

function build_parameters(config) {
	config.iterations = parseInt(document.getElementById('iterations').value)
	return config
}

function render() {
	const config = build_parameters(default_config)

	const old = find('#drawing')
	if ( old.length > 0 ) {
		old.remove()
	}

	const canvas = SVG()
		.addTo('#draw-container')
		.size("100%", "100%")
		.viewbox(0, 0, config.size, config.size)
		.attr('id', 'drawing')

	golden_dragon(canvas, config)
}

document.forms[0].querySelectorAll('input')
	.forEach(input => input.onchange = render)

const iterationsField = document.getElementById('iterations')
document.getElementById('iterations-dec').onclick = (event) => {
	if (iterationsField.value > iterationsField.min) {
		iterationsField.value--
		render()
	}
}
document.getElementById('iterations-inc').onclick = (event) => {
	console.log(iterationsField)
	if (iterationsField.value < iterationsField.max) {
		iterationsField.value++
		render()
	}
}

let last_download
document.getElementById('download').onclick = (event) => {
	if (last_download) {
		URL.revokeObjectURL(last_download)
	}
	const canvas = SVG('#drawing')
	if (!canvas) {
		throw Error("No drawing to export")
	}
	const a = document.createElement('a')
	a.href = URL.createObjectURL(new Blob([canvas.svg()]))
	a.download = 'golden-dragon.svg'
	last_download = a.href
	a.click()
}

document.addEventListener("DOMContentLoaded", () => {
	document.getElementById('noscript').hidden = true
	render()
})

