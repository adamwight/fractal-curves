// Calculations from https://larryriddle.agnesscott.org/ifs/heighway/goldenDragon.htm
const A = Math.PI * 32.893818 / 180;
const B = Math.PI * 46.98598225 / 180;
const C = Math.PI - A - B;
const R = 0.74274;

export function golden_dragon(canvas, config) {
	const initialCursor = {
		x: config.size / 4,
		y: config.size / 2,
		length: config.size / 2,
		angle: 0
	}

	let polars = [initialCursor];

	for (let i = 0; i < config.iterations; i++) {
		let sign = 1;
		polars = polars.flatMap(cursor => {
			if (sign > 0) {
				sign = -sign
				return [
					{
						length: cursor.length * R,
						angle: cursor.angle - A
					},
					{
						length: cursor.length * R * R,
						angle: cursor.angle + (Math.PI - A - C),
					}
				]
			} else {
				sign = -sign
				return [
					{
						length: cursor.length * R * R,
						angle: cursor.angle + B,
					},
					{
						length: cursor.length * R,
						angle: cursor.angle + (B + C - Math.PI),
					}
				]
			}
		})
	}

	const coords = []
	polars.reduce(
		(cursor, turn) => {
			const next = {
				x: cursor.x + turn.length * Math.cos(turn.angle),
				y: cursor.y + turn.length * Math.sin(turn.angle),
			}
			coords.push(next)
			return next
		},
		initialCursor
	)

	const svgPath = [`M ${initialCursor.x} ${initialCursor.y}`].concat(
		coords.map(next => `L ${next.x} ${next.y}`)
	)

	canvas.group()
		.path(svgPath.join('\n'))
		.fill('none')
		.stroke({width: config.hairline, color: '#000'})
		.back()
}
