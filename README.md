Draw fractals

Demo site: https://adamwight.gitlab.io/fractal-curves/

Running
===
Rebuild styles (add `-w` to watch continually):

  yarn run tailwindcss build -i input.css -o public/output.css

Host locally:

  python3 -m http.server --directory public/
